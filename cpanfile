requires "perl" => "5.006";

on 'test' => sub {
  requires "Test::More" => "0.47";
  requires "perl" => "5.006";
};

on 'configure' => sub {
  requires "ExtUtils::MakeMaker" => "0";
  requires "perl" => "5.006";
};
